import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import './App.css';
import MainContainer from './containers/MainContainer/MainContainer';
import AddNewContact from './containers/AddNewContact/AddNewContact';
import Layout from './components/UI/Layout/Layout';
import Edit from "./containers/EditContact/Edit";

class App extends Component {
  render() {
    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={MainContainer}/>
                <Route path="/new" component={AddNewContact}/>
                <Route path="/edit/:id" component={Edit}/>
            </Switch>
        </Layout>
    );
  }
}

export default App;
