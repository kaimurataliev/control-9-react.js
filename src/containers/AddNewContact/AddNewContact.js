import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';

import './AddNewContact.css';
import {addNewContactToBase} from "../../store/actions";

class AddNewContact extends Component {

    state = {
        name: '',
        number: '',
        email: '',
        photo: ''
    };

    getName = (e) => {
        this.setState({name: e.target.value});
    };

    getNumber = (e) => {
        this.setState({number: e.target.value});
    };

    getEmail = (e) => {
        this.setState({email: e.target.value});
    };

    getPhoto = (e) => {
        this.setState({photo: e.target.value});
    };

    addContactHandler = (event) => {
        event.preventDefault();
        this.props.addNewContactToBase(this.state);
        this.setState({name: '', number: '', email: '', photo: ''});
    };

    render() {
        return (
            <Fragment>
                <form className="form">
                    <h1>Add new contact</h1>

                    <label htmlFor="name">Enter name</label>
                    <input
                        id="name"
                        type="text"
                        value={this.state.name}
                        onChange={this.getName}
                    />

                    <label htmlFor="number">Enter number</label>
                    <input
                        id="number"
                        type="text"
                        value={this.state.number}
                        onChange={this.getNumber}
                    />

                    <label htmlFor="email">Enter email</label>
                    <input
                        id="email"
                        type="text"
                        value={this.state.email}
                        onChange={this.getEmail}
                    />

                    <label htmlFor="photo">Plaese enter photo URL</label>
                    <input
                        id="photo"
                        type="text"
                        value={this.state.photo}
                        onChange={this.getPhoto}
                    />

                    <div className="photo">
                        <img className="preview" src={this.state.photo}/>
                    </div>

                    <button onClick={this.addContactHandler}>Add new contact</button>
                    <NavLink className="back-to-contacts" to="/">Back to contacts</NavLink>
                </form>
            </Fragment>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addNewContactToBase: (contact) => dispatch(addNewContactToBase(contact))
    }
};

export default connect(null, mapDispatchToProps)(AddNewContact);