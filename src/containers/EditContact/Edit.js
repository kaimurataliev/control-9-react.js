import React, { Component } from 'react';
import './Edit.css';
import {editContact} from "../../store/actions";
import connect from "react-redux/es/connect/connect";
import axios from '../../axios';
import { NavLink } from 'react-router-dom';

class Edit extends Component {

    state = {
        name: '',
        email: '',
        number: '',
        photo: ''
    };

    componentDidMount() {
        this.getInfo();
    }

    getInfo = () => {
        axios.get(`./contacts/${this.props.match.params.id}.json`)
            .then(response => {
                this.setState({
                    name: response.data.name,
                    email: response.data.email,
                    number: response.data.number,
                    photo: response.data.photo
                })
            })
    };


    getName = (e) => {
        this.setState({name: e.target.value});
    };

    getNumber = (e) => {
        this.setState({number: e.target.value});
    };

    getEmail = (e) => {
        this.setState({email: e.target.value});
    };

    getPhoto = (e) => {
        this.setState({photo: e.target.value});
    };

    editHandler = (event, id, data) => {
        event.preventDefault();
        this.props.editContact(id, data);
        this.setState({
            name: '',
            email: '',
            number: '',
            photo: ''
        })
    };

    render() {
        return (
            <form className="editForm">
                <h1>Edit contact</h1>

                <label htmlFor="name">Edit name</label>
                <input
                    id="name"
                    type="text"
                    value={this.state.name}
                    onChange={this.getName}
                />

                <label htmlFor="number">Edit number</label>
                <input
                    id="number"
                    type="text"
                    value={this.state.number}
                    onChange={this.getNumber}
                />

                <label htmlFor="email">Edit email</label>
                <input
                    id="email"
                    type="text"
                    value={this.state.email}
                    onChange={this.getEmail}
                />

                <label htmlFor="photo">Edit photo url</label>
                <input
                    id="photo"
                    type="text"
                    value={this.state.photo}
                    onChange={this.getPhoto}
                />

                <div className='editPhoto'>
                    <img className="editPreview" src={this.state.photo}/>
                </div>

                <button onClick={(event) => this.editHandler(event, this.props.match.params.id, this.state)}>Edit contact</button>
                <NavLink className="back" to="/">Back to contacts</NavLink>
            </form>
        )
    }
}


const mapDispatchToProps = dispatch => {
    return {
        editContact: (id, data) => dispatch(editContact(id, data))
    }
};

export default connect(null, mapDispatchToProps)(Edit);