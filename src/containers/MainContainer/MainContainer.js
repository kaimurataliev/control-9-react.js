import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from '../../axios';

import './MainContainer.css';
import Contact from '../../components/Contact/Contact';
import {deleteContactFromBase, editContact, fetchContacts} from "../../store/actions";
import Modal from '../../components/UI/Modal/Modal';

class MainContainer extends Component {

    state = {
        id: '',
        contact: {},
        showModal: false
    };

    componentDidMount() {
        this.props.fetchContacts()
    }

    toggleModal = (event) => {
        event.preventDefault();
        this.setState({showModal: !this.state.showModal})
    };

    showInfo = (e, id) => {
        e.preventDefault();
        axios.get(`./contacts/${id}.json`).then(response => {
            this.setState({contact: response.data, id, showModal: true});
        })
    };

    deleteContactHandler = (event, id) => {
        event.preventDefault();
        this.props.deleteContactFromBase(id);
        this.setState({showModal: false});
        this.props.fetchContacts();
    };


    render() {
        return (
            <div className="container">
                <h1>list of contacts</h1>
                <Modal
                    deleteContactHandler={(e) => this.deleteContactHandler(e, this.state.id)}
                    toggleModal={this.toggleModal}
                    contact={this.state.contact}
                    show={this.state.showModal}
                    id={this.state.id}
                />

                <Contact
                    showInfo={this.showInfo}
                    contacts={this.props.contacts}
                    contact={this.state.contact}
                />

            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        contacts: state.contacts
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchContacts: () => dispatch(fetchContacts()),
        deleteContactFromBase: (id) => dispatch(deleteContactFromBase(id)),
        editContact: (id, data) => dispatch(editContact(id, data))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer);