import axios from '../axios';

export const FETCH_REQUEST = "FETCH_REQUEST";
export const FETCH_ERROR = "FETCH_ERROR";
export const FETCH_CONTACTS_SUCCESS = "FETCH_CONTACTS_SUCCESS";

export const fetchRequest = () => {
    return {type: FETCH_REQUEST}
};

export const fetchError = (error) => {
    return {type: FETCH_ERROR, error}
};

export const fetchContactsSuccess = (data) => {
    return {type: FETCH_CONTACTS_SUCCESS, data};
};


export const editContact = (id, data) => {
    return (dispatch) => {
        dispatch(fetchRequest());
        axios.put(`./contacts/${id}.json`, data)
    }
};

export const fetchContacts = () => {
    return (dispatch) => {
        dispatch(fetchRequest());
        axios.get('./contacts.json')
            .then(response => {
                dispatch(fetchContactsSuccess(response.data));
            }, error => {
                dispatch(fetchError(error))
            })
    }
};

export const deleteContactFromBase = (id) => {
    return (dispatch) => {
        dispatch(fetchRequest());
        axios.delete(`./contacts/${id}.json`)
            .then(dispatch(fetchContacts()))
    }
};

export const addNewContactToBase = (contact) => {
    return (dispatch) => {
        dispatch(fetchRequest());
        axios.post('./contacts.json', contact);
    }
};
