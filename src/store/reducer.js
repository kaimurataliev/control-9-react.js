import * as actions from './actions';

const initialState = {
    contacts: {},
    isLoading: false,
    error: false
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actions.FETCH_REQUEST:
            return {...state, isLoading: true};

        case actions.FETCH_ERROR:
            return {...state, error: true};

        case actions.FETCH_CONTACTS_SUCCESS:
            return {...state, contacts: action.data};

        default:
            return state;
    }
};

export default reducer;