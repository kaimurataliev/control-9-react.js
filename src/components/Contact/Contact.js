import React, { Fragment }  from 'react';
import './Contact.css';

const Contact = props => {

    const contacts = Object.keys(props.contacts);

    return (
        <Fragment>
            {contacts.map((id, index) => {
                return (
                    <div className="contact" key={index}>
                        <img src={props.contacts[id].photo} alt="photo"/>
                        <p>Name: {props.contacts[id].name}</p>
                        <button onClick={(e) => props.showInfo(e, id)}>Info</button>
                    </div>
                )
            })}
        </Fragment>
    )
};

export default Contact;