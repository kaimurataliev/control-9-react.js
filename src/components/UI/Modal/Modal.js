import React, { Fragment } from 'react';
import './Modal.css';
import { NavLink } from 'react-router-dom';

const Modal = props => {
    const link = `/edit/${props.id}`;
    return (
        <Fragment>
            <div className="Modal" style={{
                transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                opacity: props.show ? '1' : '0'
            }}
            >
                <img src={props.contact.photo}/>
                <h4>name: {props.contact.name}</h4>
                <p>email: {props.contact.email}</p>
                <i>number: {props.contact.number}</i>
                <button onClick={(event) => props.toggleModal(event)}>close modal</button>
                <button onClick={(event, id) => props.deleteContactHandler(event, id)}>delete contact</button>
                <NavLink className='editButton' to={link}>Edit</NavLink>
            </div>
        </Fragment>
    )
};

export default Modal;