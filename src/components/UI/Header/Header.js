import React from 'react';
import { NavLink } from 'react-router-dom';
import './Header.css';

const Header = () => {
    return (
        <header className="Toolbar">
            <nav>
                <NavLink className="link" to="/new">Add new contact</NavLink>
                <NavLink className="link" to="/">Contacts</NavLink>
            </nav>
        </header>
    )
};

export default Header;