import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://control-9.firebaseio.com/'
});


export default instance;